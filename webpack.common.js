const path = require('path');



module.exports = {
    entry: {
        main: "./src/index.js",
        // vendor: "./src/vendor.js"
    },

    module: {
        rules: [
            {
                test: /\.html$/,
                use: "html-loader"
            },
            {
                test:/\.(svg|png|jpg|gif)$/,
                use: {
                    loader: "file-loader",
                    options: {
                        name: "[name].[hash].[ext]",
                        outputPath: "imgs"
                    }
                }
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]'
                        }
                    }
                ],
                exclude: path.resolve(__dirname, './src/index.html')
            }
        ]
    }

}